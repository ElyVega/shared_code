﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.ComponentModel;

namespace pdftodb
{
    public partial class f_pdf_to_db_parser : Form
    {
        private Int32 currY = 0;
        private Int32 totalPages = 0;
        private Boolean priceCalculating = false;

        ProgressBarForm pbf;

        public f_pdf_to_db_parser()
        {
            InitializeComponent();
            pbf = new ProgressBarForm();
            totalPages = getTotalPages();
            l_pages_count.Text = totalPages.ToString();
            FileInfo fi = new FileInfo("book.sqlite");
            l_db_size.Text = (bytesToMbytes(fi.Length)).ToString("F2") + " Mb";
            tp_contents.HorizontalScroll.Maximum = 0;
            tp_contents.AutoScroll = false;
            tp_contents.VerticalScroll.Visible = false;
            tp_contents.AutoScroll = true;
            tb_price_management.HorizontalScroll.Maximum = 0;
            tb_price_management.AutoScroll = false;
            tb_price_management.VerticalScroll.Visible = false;
            tb_price_management.AutoScroll = true;
        }

        private void b_parse_pdf_Click(object sender, EventArgs e)
        {
            if (!File.Exists(tb_file_path.Text))
            {
                tb_file_path.BackColor = Color.Pink;
                MessageBox.Show("File not found!");
                return;
            } else
            {
                tb_file_path.BackColor = Color.White;
            }
            DialogResult dr = MessageBox.Show("DB will be cleared. Are you sure?", "Warning", MessageBoxButtons.YesNo);
            if (dr == DialogResult.No)
            {
                return;
            }
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            using (PdfReader reader = new PdfReader(ofd_main.FileName))
            {
                PdfReader.unethicalreading = true;
                totalPages = reader.NumberOfPages;
                pb_pdf_parser.Maximum = totalPages;
                pb_pdf_parser.Minimum = 0;
                pb_pdf_parser.Value = 0;
                pb_pdf_parser.Step = 1;
                conn.Open();
                cmd.CommandText = "DELETE FROM books";
                cmd.ExecuteNonQuery();
                for (int pagenumber = 1; pagenumber <= totalPages; pagenumber++)
                {
                    Document document = new Document();
                    PdfCopy copy = new PdfCopy(document, new FileStream(@"D:\z\Result_" + pagenumber + ".pdf", FileMode.Create));
                    document.Open();
                    copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                    document.Close();
                    Byte[] content = GetBLOB(@"D:\z\Result_" + pagenumber + ".pdf");
                    cmd.CommandText = "INSERT INTO books (id,content,access) VALUES (@id,@content,@access)";
                    cmd.Parameters.AddWithValue("@id", pagenumber);
                    cmd.Parameters.AddWithValue("@content", content);
                    cmd.Parameters.AddWithValue("@access", 0);
                    cmd.ExecuteNonQuery();
                    pb_pdf_parser.PerformStep();
                }
                cmd.CommandText = "VACUUM";
                cmd.ExecuteNonQuery();
                l_pages_count.Text = totalPages.ToString();
            }
            FileInfo fi = new FileInfo("book.sqlite");
            l_db_size.Text = (bytesToMbytes(fi.Length)).ToString("F2") + " Mb";
        }

        private void b_fd_Click(object sender, EventArgs e)
        {
            ofd_main.ShowDialog();
            if (File.Exists(ofd_main.FileName))
            {
                tb_file_path.Text = ofd_main.FileName;
                b_parse_pdf.Enabled = true;
            }
        }

        private byte[] GetBLOB(String filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Byte[] blob = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();
            return blob;
        }

        private void b_show_page_Click(object sender, EventArgs e)
        {
            Int32 page = 1;
            if (tb_show_page.Text != String.Empty)
            {
                Int32.TryParse(tb_show_page.Text, out page);
            } else
            {
                tb_show_page.Text = "1";
            }
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT content FROM books WHERE id = " + page.ToString();
                SQLiteDataReader reader = cmd.ExecuteReader();
                reader.Read();
                pdf_viewer.LoadDocument((byte[])reader["content"]);
                pdf_viewer.Padding = new Padding(0,0,0,0);
                pdf_viewer.Visible = true;
                reader.Close();
            }
        }

        private void b_add_chapter_Click(object sender, EventArgs e)
        {
            TextBox tb = new TextBox();
            tb.Location = new Point(10, b_add_chapter.Location.Y);
            tb.Size = new Size(660, 26);
            tp_contents.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(676, b_add_chapter.Location.Y);
            tb.Size = new Size(49, 26);
            tp_contents.Controls.Add(tb);
            b_add_chapter.Location = new Point(b_add_chapter.Location.X, b_add_chapter.Location.Y + 30);
            b_page.Location = new Point(b_page.Location.X, b_page.Location.Y + 30);
            b_add_paragraph.Location = new Point(b_add_paragraph.Location.X, b_add_paragraph.Location.Y + 30);
            b_page.Enabled = true;
            b_add_paragraph.Enabled = true;
            tp_contents.ScrollControlIntoView(b_add_chapter);
        }

        private void b_page_Click(object sender, EventArgs e)
        {
            if (tp_contents.Controls.Count > 7)
            {
                tp_contents.Controls[tp_contents.Controls.Count - 1].Dispose();
                tp_contents.Controls[tp_contents.Controls.Count - 1].Dispose();
                b_add_chapter.Location = new Point(b_add_chapter.Location.X, b_add_chapter.Location.Y - 30);
                b_page.Location = new Point(b_page.Location.X, b_page.Location.Y - 30);
                b_add_paragraph.Location = new Point(b_add_paragraph.Location.X, b_add_paragraph.Location.Y - 30);
                if (tp_contents.Controls.Count <= 10)
                {
                    b_page.Enabled = false;
                    b_add_paragraph.Enabled = false;
                }
            }
        }

        private void b_save_contents_Click(object sender, EventArgs e)
        {
            Int32 prepages;
            if (!Int32.TryParse(tb_pages_count.Text, out prepages))
            {
                tb_pages_count.BackColor = Color.Pink;
                MessageBox.Show("Wrong page number!");
                return;
            }
            clearPrices();
            pb_contents.Maximum = tp_contents.Controls.Count;
            pb_contents.Minimum = 0;
            pb_contents.Value = 0;
            pb_contents.Step = 1;
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "DELETE FROM contents";
                cmd.ExecuteNonQuery();
                String conText = String.Empty;
                Int32 baseWidth = 0;
                Int32 conPage = 0;
                Int32 conId = 1;
                Int32 currParent = 0;
                Int32 conParent = 0;
                currY = tb_price_management.Controls[tb_price_management.Controls.Count - 1].Location.Y + 24;
                Int32 prevChapterId = 0;
                Int32 prevChapterPage = 0;
                String prevChapterText = String.Empty;
                Int32 chapterCounter = 1;
                Int32 chapterPages = 0;
                Int32 freePages = 0;
                foreach (Control cc in tp_contents.Controls)
                {
                    if ((cc is TextBox)&&(cc.Name != "prepagesCount"))
                    {
                        cc.BackColor = Color.White;
                        if (cc.Text == String.Empty)
                        {
                            cc.BackColor = Color.Pink;
                            MessageBox.Show("Empty value is not allowed!");
                            return;
                        }
                        if (conText == String.Empty)
                        {
                            if (baseWidth == 0)
                            {
                                baseWidth = cc.Width;
                            }
                            conText = cc.Text;
                            if (cc.Width < baseWidth)
                            {
                                conParent = currParent;
                            } else
                            {
                                currParent = conId;
                                conParent = 0;
                            }
                        } else if (conPage == 0)
                        {
                            if (Int32.TryParse(cc.Text, out conPage))
                            {
                                if (conParent == 0)
                                {
                                    if (prevChapterPage != 0)
                                    {
                                        chapterPages = conPage + prepages - prevChapterPage;
                                        if (chapterCounter > 5)
                                        {
                                            freePages = Convert.ToInt32(Math.Floor(chapterPages / 5.0));
                                            if (freePages < 1)
                                            {
                                                freePages = 1;
                                            }
                                        } else
                                        {
                                            freePages = conPage + prepages - prevChapterPage;
                                        }
                                        addPriceOption(prevChapterId, prevChapterText, chapterPages, freePages, 0);
                                        chapterCounter++;
                                    }
                                    prevChapterId = conId;
                                    prevChapterPage = conPage + prepages;
                                    prevChapterText = conText;
                                }
                                cmd.CommandText = "INSERT INTO contents (id,text,page,parent_id) VALUES (@id,@text,@page,@parent_id)";
                                cmd.Parameters.AddWithValue("@id", conId);
                                cmd.Parameters.AddWithValue("@text", conText);
                                cmd.Parameters.AddWithValue("@page", conPage + prepages);
                                cmd.Parameters.AddWithValue("@parent_id", conParent);
                                cmd.ExecuteNonQuery();
                                conText = String.Empty;
                                conPage = 0;
                                conId++;
                            }
                            else
                            {
                                cc.BackColor = Color.Pink;
                                MessageBox.Show("Wrong page number!");
                                return;
                            }
                        } else
                        {
                            MessageBox.Show("Something went wrong!");
                            return;
                        }
                    }
                    pb_contents.PerformStep();
                }
                chapterPages = totalPages - prevChapterPage + 1;
                if (chapterCounter > 5)
                {
                    freePages = Convert.ToInt32(Math.Floor(chapterPages / 5.0));
                    if (freePages < 1)
                    {
                        freePages = 1;
                    }
                }
                else
                {
                    freePages = conPage + prepages - prevChapterPage;
                }
                addPriceOption(prevChapterId, prevChapterText, chapterPages, freePages, 0);
                MessageBox.Show("Done!");
            }
        }

        private void b_load_contents_Click(object sender, EventArgs e)
        {
            if (b_page.Enabled)
            {
                DialogResult dr = MessageBox.Show("Contents changes will be overwrited. Continue?", "Warning!", MessageBoxButtons.YesNo);
                if (dr == DialogResult.No)
                {
                    return;
                }
            }
            while (b_page.Enabled)
            {
                b_page.PerformClick();
            }
            tb_pages_count.Enabled = false;
            clearPrices();
            currY = tb_price_management.Controls[tb_price_management.Controls.Count - 1].Location.Y + 24;
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT id, text, page, parent_id, price FROM contents";
                SQLiteDataReader reader = cmd.ExecuteReader();
                Int32 conId = 0;
                String chapterText = String.Empty;
                Int32 page = 0;
                Double price = 0;
                while (reader.Read())
                {
                    if (reader["parent_id"].ToString() == "0")
                    {
                        b_add_chapter.PerformClick();
                        tp_contents.Controls[tp_contents.Controls.Count - 1].Text = reader["page"].ToString();
                        tp_contents.Controls[tp_contents.Controls.Count - 2].Text = reader["text"].ToString();
                        Int32 currPage = 0;
                        Int32.TryParse(reader["page"].ToString(), out currPage);
                        if (page != 0)
                        {
                            addPriceOption(conId, chapterText, currPage - page, 0, price);
                        }
                        Int32.TryParse(reader["id"].ToString(), out conId);
                        chapterText = reader["text"].ToString();
                        page = currPage;
                        Double.TryParse(reader["price"].ToString(), out price);
                    }
                    else
                    {
                        b_add_paragraph.PerformClick();
                        tp_contents.Controls[tp_contents.Controls.Count - 1].Text = reader["page"].ToString();
                        tp_contents.Controls[tp_contents.Controls.Count - 2].Text = reader["text"].ToString();
                    }
                }
                reader.Close();
                cmd.CommandText = "SELECT SUM(access) FROM books WHERE id >= @pstart";
                cmd.Parameters.AddWithValue("@pstart", page);
                addPriceOption(conId, chapterText, totalPages - page + 1, Int32.Parse(cmd.ExecuteScalar().ToString()), price);
                for (Int32 i = 0; i < tb_price_management.Controls.Count - 6; i++)
                {
                    if (!(tb_price_management.Controls[i] is TextBox) || (tb_price_management.Controls[i].Size.Width != 20) || (tb_price_management.Controls[i].Name == "total"))
                    {
                        continue;
                    }
                    cmd.CommandText = "SELECT SUM(access) FROM books WHERE id >= (SELECT page FROM contents WHERE id = @pstart) AND id < (SELECT page FROM contents WHERE id = @pend)";
                    cmd.Parameters.AddWithValue("@pstart", tb_price_management.Controls[i].Text);
                    cmd.Parameters.AddWithValue("@pend", tb_price_management.Controls[i + 5].Text);
                    tb_price_management.Controls[i + 3].Text = cmd.ExecuteScalar().ToString();
                    
                }
            }
        }

        private void b_add_paragraph_Click(object sender, EventArgs e)
        {
            TextBox tb = new TextBox();
            tb.Location = new Point(50, b_add_chapter.Location.Y);
            tb.Size = new Size(620, 26);
            tp_contents.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(676, b_add_chapter.Location.Y);
            tb.Size = new Size(49, 26);
            tp_contents.Controls.Add(tb);
            b_add_chapter.Location = new Point(b_add_chapter.Location.X, b_add_chapter.Location.Y + 30);
            b_page.Location = new Point(b_page.Location.X, b_page.Location.Y + 30);
            b_add_paragraph.Location = new Point(b_add_paragraph.Location.X, b_add_paragraph.Location.Y + 30);
            b_page.Enabled = true;
            tp_contents.ScrollControlIntoView(b_add_paragraph);
        }

        private void b_erase_contents_Click(object sender, EventArgs e)
        {

        }

        private void clearPrices()
        {
            for (Int32 i = tb_price_management.Controls.Count - 1; i >= 0; i--)
            {
                if ((tb_price_management.Controls[i] is TextBox)&&(tb_price_management.Controls[i].Name != "total"))
                {
                        tb_price_management.Controls[i].Dispose();
                }
            }
        }

        private void addPriceOption(Int32 conId, String chapterText, Int32 pages, Int32 freePages, Double price)
        {
            currY += 30;
            TextBox tb;
            tb = new TextBox();
            tb.Location = new Point(10, currY);
            tb.Size = new Size(20, 26);
            tb.Visible = false;
            tb.Text = conId.ToString();
            tb_price_management.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(10, currY);
            tb.Size = new Size(540, 26);
            tb.ReadOnly = true;
            tb.Enabled = false;
            tb.Text = chapterText;
            tb_price_management.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(556, currY);
            tb.Size = new Size(50, 26);
            tb.ReadOnly = true;
            tb.Enabled = false;
            tb.Text = pages.ToString();
            tb_price_management.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(612, currY);
            tb.Size = new Size(50, 26);
            tb.Text = freePages.ToString();
            if (pages == freePages)
            {
                tb.BackColor = Color.LightGreen;
            }
            tb.TextChanged += new EventHandler(priceCheck);
            tb_price_management.Controls.Add(tb);
            tb = new TextBox();
            tb.Location = new Point(668, currY);
            tb.Size = new Size(60, 26);
            tb.Text = price.ToString();
            tb.TextChanged += new EventHandler(priceCheck);
            tb_price_management.Controls.Add(tb);
        }

        private Int32 getTotalPages()
        {
            Int32 result = 0;
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT count(*) AS total FROM books";
                SQLiteDataReader reader = cmd.ExecuteReader();
                reader.Read();
                Int32.TryParse(reader["total"].ToString(), out result);
                reader.Close();
            }
            return result;
        }

        private void b_save_prices_Click(object sender, EventArgs e)
        {
            bw_db_worker.RunWorkerAsync();
            pbf.ShowDialog();
        }

        private void f_pdf_to_db_parser_FormClosing(Object sender, FormClosingEventArgs e)
        {
            bw_progress_bar.RunWorkerAsync();
            foreach (Control cc in this.Controls)
            {
                cc.Enabled = false;
            }
            pbf.ShowDialog();
        }

        private void bw_progress_bar_DoWork(object sender, DoWorkEventArgs e)
        {
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "VACUUM";
                cmd.ExecuteNonQuery();
            }
        }

        private void bw_progress_bar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbf.Close();
        }

        private Double bytesToMbytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        private void priceCheck(object sender, EventArgs e)
        {
            if (priceCalculating)
            {
                return;
            }
            priceCalculating = true;
            Int32 totalPaidPages = 0;
            for (Int32 i = 0; i < tb_price_management.Controls.Count - 4; i++)
            {
                if (!(tb_price_management.Controls[i] is TextBox) || (tb_price_management.Controls[i].Size.Width != 20) || (tb_price_management.Controls[i].Name == "total"))
                {
                    continue;
                }
                if (tb_price_management.Controls[i + 2].Text != tb_price_management.Controls[i + 3].Text)
                {
                    tb_price_management.Controls[i + 4].Enabled = true;
                    Int32 chapterPages = 0;
                    Int32.TryParse(tb_price_management.Controls[i + 2].Text, out chapterPages);
                    totalPaidPages += chapterPages;
                    tb_price_management.Controls[i + 3].BackColor = Color.White;
                } else
                {
                    tb_price_management.Controls[i + 3].BackColor = Color.LightGreen;
                    tb_price_management.Controls[i + 4].Text = "0";
                    tb_price_management.Controls[i + 4].Enabled = false;
                }
            }
            if ((sender != null)&&(((TextBox)sender).Name == "total")){
                Double priceTotal = 0;
                if (!Double.TryParse(tb_total_price.Text, out priceTotal)){
                    tb_total_price.BackColor = Color.Pink;
                    priceCalculating = false;
                    return;
                } else
                {
                    tb_total_price.BackColor = Color.White;
                }
                Double perPage = priceTotal / totalPaidPages;
                for (Int32 i = 0; i < tb_price_management.Controls.Count - 4; i++)
                {
                    if (!(tb_price_management.Controls[i] is TextBox) || (tb_price_management.Controls[i].Size.Width != 20) || (tb_price_management.Controls[i].Name == "total"))
                    {
                        continue;
                    }
                    if (tb_price_management.Controls[i + 4].Enabled == true)
                    {
                        Int32 chapterPages = 0;
                        Int32.TryParse(tb_price_management.Controls[i + 2].Text, out chapterPages);
                        tb_price_management.Controls[i + 4].Text = (Math.Floor(chapterPages * perPage * 1.2 * 100) / 100.0).ToString();
                    }
                }
            } else {
                Double priceByChapters = 0;
                for (Int32 i = 0; i < tb_price_management.Controls.Count - 4; i++)
                {
                    if (!(tb_price_management.Controls[i] is TextBox) || (tb_price_management.Controls[i].Size.Width != 20) || (tb_price_management.Controls[i].Name == "total"))
                    {
                        continue;
                    }
                    if (tb_price_management.Controls[i + 4].Enabled == true)
                    {
                        Double chapterPrice = 0;
                        if (!Double.TryParse(tb_price_management.Controls[i + 4].Text, out chapterPrice))
                        {
                            tb_price_management.Controls[i + 4].BackColor = Color.Pink;
                        } else
                        {
                            tb_price_management.Controls[i + 4].BackColor = Color.White;
                        }
                        priceByChapters += chapterPrice;
                    }
                }
                tb_total_price.Text = Math.Ceiling(priceByChapters * 100 / 120).ToString();
            }
            priceCalculating = false;
        }

        private void bw_db_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            using (SQLiteConnection conn = new SQLiteConnection(@"Data Source=book.sqlite"))
            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "UPDATE books SET access = 0";
                cmd.ExecuteNonQuery();
                for (Int32 i = 0; i < tb_price_management.Controls.Count - 6; i++)
                {
                    if (!(tb_price_management.Controls[i] is TextBox) || (tb_price_management.Controls[i].Size.Width != 20) || (tb_price_management.Controls[i].Name == "total"))
                    {
                        continue;
                    }
                    cmd.CommandText = "UPDATE contents SET price = @price WHERE id = @id";
                    cmd.Parameters.AddWithValue("@id", tb_price_management.Controls[i].Text);
                    cmd.Parameters.AddWithValue("@price", tb_price_management.Controls[i + 4].Text);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE books SET access = 1 WHERE id >= (SELECT page FROM contents WHERE id = @id1) AND id < (SELECT page + @pcount FROM contents WHERE id = @id2)";
                    cmd.Parameters.AddWithValue("@id1", tb_price_management.Controls[i].Text);
                    cmd.Parameters.AddWithValue("@id2", tb_price_management.Controls[i].Text);
                    cmd.Parameters.AddWithValue("@pcount", tb_price_management.Controls[i + 3].Text);
                    cmd.ExecuteNonQuery();
                }
                cmd.CommandText = "UPDATE contents SET price = @price WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", tb_price_management.Controls[tb_price_management.Controls.Count - 5].Text);
                cmd.Parameters.AddWithValue("@price", tb_price_management.Controls[tb_price_management.Controls.Count - 1].Text);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE books SET access = 1 WHERE id >= (SELECT page FROM contents WHERE id = @id1) AND id < (SELECT page + @pcount FROM contents WHERE id = @id2)";
                cmd.Parameters.AddWithValue("@id1", tb_price_management.Controls[tb_price_management.Controls.Count - 5].Text);
                cmd.Parameters.AddWithValue("@id2", tb_price_management.Controls[tb_price_management.Controls.Count - 5].Text);
                cmd.Parameters.AddWithValue("@pcount", tb_price_management.Controls[tb_price_management.Controls.Count - 2].Text);
                cmd.ExecuteNonQuery();
            }
        }

        private void bw_db_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbf.Visible = false;
            pbf.Close();
            MessageBox.Show("Done!");
        }

        private void f_pdf_to_db_parser_SizeChanged(object sender, EventArgs e)
        {
            tc_main.Width = this.Width - 40;
            tc_main.Height = this.Height - 60;
        }
    }
}
