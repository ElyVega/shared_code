var commentHash = location.hash;
$('.education.exam.container .comments-block').css('display', 'block');
$(document).ready(function() {
    $('.answers li').on('click', function(){
        $('.answers li').addClass('answered');
        if ($(this).hasClass('green')){
            $(this).addClass('right_answer');
            $('#answered_right').modal('show');
        } else {
            $(this).addClass('wrong_answer');
            $('.green').addClass('right_answer');
            $('#answered_wrong').modal('show');
        }
    });
    $('.question-img .image_set').parent().css('height', ($('.image_set').height() + 2) + 'px');
    $('.question-img .image_set').on('mouseover', function () {
        $(this).addClass('hover');
    });
    $('.question-img .image_set').on('mouseleave', function () {
        $(this).removeClass('hover');
    });
    $('.addtofav.authed').on('click', function () {
        var type = $(this).hasClass('added') ? 0 : 1;
        if (!type) {
            $(this).removeClass('added');
            $(this).find(".addtofav_desc > span").html(text_favorite_add_to);
        }
        else {
            $(this).addClass('added');
            $(this).find(".addtofav_desc > span").html(text_favorite_added);
            $(".like_block .addtofav_description").addClass('show');
            setTimeout(function() {
                $(".like_block .addtofav_description").removeClass('show');
            }, 4000);
        }
        addToFav(type);
    });
    $('.like_block .like, .like_block .dislike').on('click', function(){
        $('.like, .dislike').css('pointer-events', 'none');
        var elem = $(this);
        if ($(elem).hasClass('pressed')){
            $(elem).removeClass('pressed');
            $(elem).addClass('nonpressed');
            $(elem).text(parseInt($(elem).text()) - 1);
            $('.like, .dislike').css('pointer-events', 'auto');
        }
        var isLike;
        if ($(elem).hasClass('dislike')){
            isLike = 0;
        } else {
            isLike = 1;
        }
        setLikeStatus(isLike);
        $(elem).removeClass('nonpressed');
        $(elem).addClass('pressed');
        $(elem).text(parseInt($(elem).text()) + 1);
        if (isLike){
            var dislikeElem = $('#like_block_' + qId + ' .dislike');
            if (dislikeElem.hasClass('pressed')){
                dislikeElem.removeClass('pressed').addClass('nonpressed');
                dislikeElem.text(parseInt(dislikeElem.text()) - 1);
            }
        } else {
            var likeElem = $('#like_block_' + qId + ' .like');
            if (likeElem.hasClass('pressed')){
                likeElem.removeClass('pressed').addClass('nonpressed');
                likeElem.text(parseInt(likeElem.text()) - 1);
            }
        }
        $('.like, .dislike').css('pointer-events', 'auto');
    });
    $('#container-social button').on('click', function(){
        onQuestionShare();
    });
    $('#fbShare').on('click', function() {
        window.open("https://www.facebook.com/sharer.php?u="+location.href,"","height=368,width=600,left=100,top=100,menubar=0");
        return false;
    });
    $('#gplusShare').on('click', function() {
        window.open("https://plus.google.com/share?url="+location.href,"","height=550,width=525,left=100,top=100,menubar=0");
        return false;
    });
    $('#twitterShare').on('click', function() {
        window.open("https://twitter.com/share?url="+location.href+"&text="+encodeURIComponent($('.tab-pane.active .text_question').text().trim())+' ',"","height=260,width=500,left=100,top=100,menubar=0");
        return false;
    });
    // Telegram share
    $('#tgShare').on('click', function() {
        window.open("https://t.me/share/url?url="+location.href+"&text="+encodeURIComponent($('.tab-pane.active .text_question').text().trim()));
    });
    $(this).request('onGetLikes', {
        data: {qids: qids},
        success: function(data) {
            $('.like_block').each(function(){
                var lbId = $(this).children('div').eq(0).children('div').eq(1).attr('id');
                var lqId = lbId.split('like_block_')[1];
                if (typeof data['totalLikes'][lqId] != 'undefined'){
                    $('#' + lbId + " .like").text(data['totalLikes'][lqId]['positiveLikes']);
                    $('#' + lbId + " .dislike").text(data['totalLikes'][lqId]['totalLikes'] - data['totalLikes'][lqId]['positiveLikes']);
                } else {
                    $('#' + lbId + " .like").text(0);
                    $('#' + lbId + " .dislike").text(0);
                }
                if ((typeof data['userLikes'] != 'undefined')&&(typeof data['userLikes'][lqId] != 'undefined')){
                    if (data['userLikes'][lqId] == 1){
                        $('#' + lbId + " .like").removeClass('nonpressed').addClass('pressed');
                    } else {
                        $('#' + lbId + " .dislike").removeClass('nonpressed').addClass('pressed');
                    }
                }
                $('#' + lbId).parent().parent().css('opacity', '1').css('pointer-events', 'auto');
            });
        },
        error: function(data) {}
    });
    $(this).request('onGetFavorites', {
        data: {qids: qids},
        success: function(data) {
            $('.like_block .addtofav').each(function(){
                var lbId = $(this).attr('id');
                var fqId = lbId.split('addtofav_')[1];
                if (data['userFavorites'][fqId] !== undefined && data['userFavorites'][fqId] == true ) {
                    $("#addtofav_"+fqId).addClass('added');
                    $("#addtofav_"+fqId+" .addtofav_desc > span").html(text_favorite_added);
                }
            });
        },
        error: function(data) {}
    });
    showComment('question_' + qId);
    $('.btn-no').on('click', function () {
        $('.close').click();
    });
    $('.btn-yeah').on('click', function () {
        $(this).parents('.modal-body').html('<div id="loading_block" style="display: block"><div class="info center"><div class="loading"><img src="/themes/greenway/assets/images/loading.gif" style="margin-bottom: 0px;" width="60" height="60" alt="Загрузка" title="Загрузка"></div></div></div>');
        interruptionDirection();
    });
});
function setLikeStatus(likeStatus){
    var qLikes;
    var cName = userId + "lStack";
    qLikes = cookieDecrypt(cName);
    if (qLikes === null)
        qLikes = [];
    for (var i = 0; i < qLikes.length; i++){
        if ((qLikes[i][0] == qId)&&(qLikes[i][2] == 'q')){
            qLikes[i][1] = likeStatus;
            cookieEncrypt(qLikes, cName);
            return;
        }
    }
    qLikes.push([qId, likeStatus, 'q']);
    cookieEncrypt(qLikes, cName);
}
function addToFav(type) {
    var qFavs;
    var cName = userId + "fStack";
    qFavs = cookieDecrypt(cName);
    if (qFavs === null)
        qFavs = [];
    for (var i = 0; i < qFavs.length; i++){
        if ((qFavs[i][0] == qId)&&(qFavs[i][2] == 'q')){
            qFavs.splice(i, 1);
            cookieEncrypt(qFavs, cName);
            return;
        }
    }
    qFavs.push([qId, type, 'q']);
    cookieEncrypt(qFavs, cName);
}
function cookieInc(name) {
    var value = cookieDecrypt(name);
    if (value === null){
        value = 0;
    }
    value += 1;
    cookieEncrypt(value, name);
}
function onQuestionShare(){
    if (userId != 0) {
        cookieInc(userId + "qShared");
    }
}