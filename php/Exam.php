<?php namespace Greenway\Education\Components;

use DB;
use Auth;
use Mail;
use Cache;
use Config;
use Cookie;
use Request;
use GWModules\CoursesMod;
use GWModules\TestpddMod;
use Cms\Classes\ComponentBase;
use RainLab\Translate\Classes\Translator;
use Illuminate\Support\Facades\Validator;
use Greenway\Userextension\Models\Achievment as Achievements;
use Greenway\Education\Models\ConnectionInfo as ConnectionInfos;

class Exam extends ComponentBase
{
    public $questionsResultSet;
    public $selectedLanguage;
    public $sessionId;
    public $userId;
    public $userInfo;
    public $qTotal;
    public $teleInviteExpected;
    public $courseBooster;

    public function componentDetails()
    {
        return [
            'name'        => 'greenway.education::lang.exam.name',
            'description' => 'greenway.education::lang.exam.description'
        ];
    }
    public function defineProperties()
    {
        return [
        ];
    }

    public function init(){
        $this->addComponent('Greenway\Userextension\Components\AdminPanel', 'adminpanel', []);
    }

    public function onRun()
    {
        $user = Auth::getUser();
        $this->userId = $user ? $user->id : 0;
        $translator = Translator::instance();
        $this->selectedLanguage = $translator->getLocale();
        $defaultLanguage = $translator->getDefaultLocale();
        $this->teleInviteExpected = false;
        $this->courseBooster = false;
        $qIdParam = $this->param('questionId');
        if (!empty($qIdParam)){
            $this->addJs('/plugins/greenway/education/assets/javascript/singleQuestion.js?ver=' . Config::get('greenway.education::scriptVersion'));
            $this->questionsResultSet[] = TestpddMod::getCachedQuestion($qIdParam, $this->selectedLanguage, $defaultLanguage);
            if (empty($this->questionsResultSet)){
                return $this->controller->run('404');
            }
            if ($user) {
                TestpddMod::releasePrimaryCookie($user->id);
            }
            return;
        }
        if (!$user||(!$this->page->UEUser->examAllowed)||(empty($this->page->UEUser->test_category))){
            return;
        }
        if (empty($this->page->UEUser->pop_ups)||(!property_exists(json_decode($this->page->UEUser->pop_ups), 'teleInvite'))){
            if (array_key_exists('userData', $_COOKIE)) {
                $cData = json_decode($_COOKIE['userData']);
            }
            if (isset($cData)&&(property_exists($cData, 'itt'))){
                if ($cData->itt < time()){
                    $this->teleInviteExpected = true;
                }
            } else {
                $condMet = DB::select("SELECT counter FROM greenway_userextension_rewards rew LEFT JOIN greenway_userextension_achievments ach ON (rew.achievment_id = ach.id) WHERE rew.user_id = ? AND ach.unique_key = 'exam-1-starts' AND rew.counter > 4", [$this->userId]);
                if (!empty($condMet)) {
                    $this->teleInviteExpected = true;
                }
            }
        }
        if ((!$this->teleInviteExpected)&&(empty($this->page->UEUser->pop_ups)||(!property_exists(json_decode($this->page->UEUser->pop_ups), 'crsBoost')))){
            $condMet = DB::select("SELECT counter FROM greenway_userextension_rewards rew LEFT JOIN greenway_userextension_achievments ach ON (rew.achievment_id = ach.id) WHERE rew.user_id = ? AND ach.unique_key = 'exam-success-1-times' AND rew.counter > 9", [$this->userId]);
            if (!empty($condMet)) {
                $this->courseBooster = true;
            }
        }
        $this->questionsResultSet = [];
        $category = $this->page->UEUser->test_category;
        $category = implode(';', array_unique(explode(';', str_replace('1', '', rtrim(rtrim($category, ';'), ' ')))));
        $ttype = $this->page->UEUser->test_type_id;
        $examQuestions = Cache::remember(str_replace(';', '_', $category . $ttype), 10080, function() use($category, $ttype){
            return DB::select("SELECT sections FROM greenway_testpdd_exam_questions eq LEFT JOIN greenway_testpdd_exams e ON (eq.exam_id = e.id) WHERE e.text = ? AND e.is_active = 1 AND eq.type_id = ? ORDER BY eq.id", [$category, $ttype]);
        });
        if (empty($examQuestions)) {
            return \Redirect::to('/');
        }
        $anCookie = Cookie::get($user->id . 'anltcs');
        if(empty($anCookie)||(!is_array($anCookie))){
            $anCookie = json_decode($anCookie);
        }
        if (!empty($anCookie)&&(property_exists($anCookie, 'lgd'))&&($anCookie->lgd == date("Y-m-d"))){
            $affected = DB::update("UPDATE greenway_userextension_analytics SET tested = tested + 1 WHERE user_id = ? AND event_date = CURDATE()", [$user->id]);
        }
        $this->addJs('/plugins/greenway/literature/assets/javascript/jquery.easing.min.js');
        $this->addJs('/themes/greenway/assets/javascript/mobile/jquery.touchSwipe.min.js?ver=' . Config::get('greenway.education::scriptVersion'));
        $this->addJs('/plugins/greenway/education/assets/javascript/main.js?ver=' . Config::get('greenway.education::scriptVersion'));
        if ((!$this->page->UEUser->isTeacher)&&($this->page->UEUser->autoschool_id)&&(!$this->page->UEUser->group_id)) {
            $autoschoolName = DB::select("SELECT guas.name AS autoschoolName FROM greenway_userextension_users_extra guue LEFT JOIN greenway_userextension_autoschools guas ON (guue.autoschool_id = guas.id) LEFT JOIN (SELECT gur.user_id, gur.counter FROM greenway_userextension_rewards gur LEFT JOIN greenway_userextension_achievments gua ON gur.achievment_id = gua.id WHERE gua.unique_key = 'exam-success-1-times' AND gur.user_id = ?) r ON (guue.user_id = r.user_id) WHERE guue.user_id = ? AND r.counter IS NULL LIMIT 1", [$user->id, $user->id]);
            if (!empty($autoschoolName)&&(!empty($autoschoolName[0]->autoschoolName))){
                $this->userInfo = $autoschoolName[0]->autoschoolName;
            }
        }
        $excluded = Cookie::get('exQIds');
        if (!empty($excluded)&&(!is_array($excluded))){
            $excluded = json_decode(Cookie::get('exQIds'));
        }
        if (empty($excluded)||(!is_array($excluded))||((!$this->page->UEUser->isVip)&&(!$this->page->UEUser->isTeacher))){
            $excluded = [];
        }
        $examQuestionsIds = [0];
        foreach ($examQuestions as $examQuestion) {
            $allowedSections = str_replace(';', ',', $examQuestion->sections);
            if (($this->page->UEUser->isVip)||($this->page->UEUser->isTeacher)){
                if (!empty($excluded)){
                    $excludedQuery = ' AND id NOT IN (' . implode(',', array_map('intval', $excluded)) . ')';
                } else {
                    $excludedQuery = '';
                }
                $questionIds = DB::select('SELECT id FROM greenway_testpdd_questions WHERE section_id IN (' . $allowedSections . ') AND id NOT IN (' . implode(',', $examQuestionsIds) . ')' . $excludedQuery . ' AND is_active = 1 AND deleted_at IS NULL');
                if (!empty($questionIds)){
                    $selectedQuestionId = $questionIds[random_int(0, 999999) % count($questionIds)]->id;
                }
            } else {
                $cachedQuestionIds = Cache::remember('examQbS' . str_replace(',', '_', $allowedSections), 1440, function() use ($allowedSections) {
                    $questionData = DB::select('SELECT id, section_id FROM greenway_testpdd_questions WHERE section_id IN (' . $allowedSections . ') AND is_active = 1 AND deleted_at IS NULL ORDER BY id ASC');
                    $questionsBySections = [];
                    foreach ($questionData as $questionToSection){
                        $questionsBySections[$questionToSection->section_id][] = $questionToSection->id;
                    }
                    $questionIds = [];
                    foreach ($questionsBySections as $questionsBySection){
                        if (count($questionsBySection) > Config::get('greenway.education::nonproQuestionLimit')){
                            $questionIds = array_merge($questionIds, array_slice($questionsBySection, 0, ceil(count($questionsBySection) * 30.0 / 100.0)));
                        } else {
                            $questionIds = array_merge($questionIds, $questionsBySection);
                        }
                    }
                    return $questionIds;
                });
                $questionIds = array_values(array_diff($cachedQuestionIds, $examQuestionsIds, $excluded));
                if (!empty($questionIds)) {
                    $selectedQuestionId = $questionIds[random_int(0, 999999) % count($questionIds)];
                }
            }
            if (empty($questionIds)){
                return;
            }
            $examQuestionsIds[] = $selectedQuestionId;
        }
        unset($examQuestionsIds[0]);
        foreach ($examQuestionsIds as $examQuestionsId) {
            $this->questionsResultSet[] = TestpddMod::getCachedQuestion($examQuestionsId, $this->selectedLanguage, $defaultLanguage);
        }
        $this->qTotal = count($this->questionsResultSet);
        $exclusionUpdate = array_merge($excluded, $examQuestionsIds);
        $exclCount = count($exclusionUpdate);
        if ($exclCount > 60){
            $exclusionUpdate = array_slice($exclusionUpdate, $exclCount - 60);
        }
        TestpddMod::releasePrimaryCookie($user->id);
        $requestTime = Request::server('REQUEST_TIME');
        $this->sessionId = ConnectionInfos::insertGetId(['user_id' => $user->id, 'client_ip' => Request::server('REMOTE_ADDR'), 'client_agent' => Request::server('HTTP_USER_AGENT'), 'request_time' => $requestTime, 'lang' => $this->selectedLanguage, 'source' => 'E']);
        Cookie::queue(Cookie::make('exQIds', json_encode($exclusionUpdate), 1440, '/', null, false, false));
        session_start();
        $_SESSION['testingData'] = array('sd' => $requestTime, 'sessionId' => $this->sessionId);
        $_SESSION['answerStack'] = array();
    }
    public function onTestFinish(){
        $user = Auth::getUser();
        if (!$user){
            return ['error' => 'nonunique account'];
        }
        $validation = Validator::make(post(), [
            'tt' => 'required|numeric'
        ]);
        if ($validation->fails()) {
            return ['error' => $validation->failed()];
        }
        session_start();
        if ((!array_key_exists('answerStack', $_SESSION))||(!array_key_exists('testingData', $_SESSION))||(empty($_SESSION['testingData']))){
            return ['error' => 'corrupted session'];
        }
        if (empty($_SESSION['answerStack'])){
            return ['url' => '/test-pdd'];
        }
        $ansStack = $_SESSION['answerStack'];
        unset($_SESSION['answerStack']);
        $ansStack = TestpddMod::mergeAnswerStack($ansStack, Cookie::get($user->id . 'AStack'));
        $testingData = $_SESSION['testingData'];
        unset($_SESSION['testingData']);
        $spendTime = TestpddMod::calculateSpendTime($testingData['sd'], Request::server('REQUEST_TIME'), post('tt'));
        $answersData = TestpddMod::generateAnswersData($ansStack);
        $crOffer = '';
        TestpddMod::writeAnswerData($user->id, $ansStack);
        $rewards = [];
        array_push($rewards,'exam-1-starts', 'exam-20-starts', 'exam-100-starts', 'exam-500-starts');
        if (($answersData['totalWA'] < 3)&&(($answersData['totalRA'] + $answersData['totalWA']) >= 20)) {
            $affected = DB::update("UPDATE greenway_education_connection_info SET is_completed = 1, completed_at = ?, is_cookie = 0 WHERE id = ?",[Request::server('REQUEST_TIME'), $testingData['sessionId']]);
            if (($spendTime > 1197)&&($spendTime <= 1200)){
                $rewards[] = 'exam-done-1-sec-before-end';
            }
            if (($spendTime < 1197)&&($spendTime > 1193)){
                $rewards[] = 'exam-done-5-sec-before-end';
            }if ($spendTime > 1200){
                $spendTime = 1200;
            }
            array_push($rewards,'exam-success-1-times','exam-success-5-times','exam-success-20-times','exam-success-50-times','exam-success-100-times','exam-success-500-times','exam-success-1000-times');
            if ($answersData['totalWA'] == 0) {
                array_push($rewards,'exam-perfect-1-times','exam-perfect-5-times','exam-perfect-20-times','exam-perfect-100-times','exam-perfect-300-times');
            }
            if ($spendTime <= 360) {
                $rewards[] = 'exam-done-in-360-seconds';
                if ($spendTime <= 240) {
                    $rewards[] = 'exam-done-in-240-seconds';
                    if ($spendTime <= 120) {
                        $rewards[] = 'exam-done-in-120-seconds';
                        if ($spendTime <= 60) {
                            $rewards[] = 'exam-done-in-60-seconds';
                            if ($spendTime <= 30) {
                                $rewards[] = 'exam-done-in-30-seconds';
                            }
                        }
                    }
                }
            }
            $cityRelatedOfferCond = DB::select("SELECT ue.city_id, p1.id AS pr1, p2.id AS pr2 FROM greenway_userextension_users_extra ue LEFT JOIN greenway_userextension_rewards r ON (ue.user_id = r.user_id) LEFT JOIN greenway_userextension_achievments ach ON (r.achievment_id = ach.id) LEFT JOIN greenway_userextension_promocodes p1 ON (ue.promocode = p1.text) LEFT JOIN greenway_userextension_groups gr ON (ue.group_id = gr.id) LEFT JOIN greenway_userextension_promocodes p2 ON (gr.user_id = p2.user_id) WHERE ue.user_id = ? AND ue.cr_course IS NULL AND ach.unique_key = 'exam-success-1-times' AND r.counter > 1 LIMIT 1", [$user->id]);
            if (!empty($cityRelatedOfferCond)){
                $translator = Translator::instance();
                $selectedLanguage = $translator->getLocale();
                $defaultLanguage = $translator->getDefaultLocale();
                $localesRaw = CoursesMod::getRawLocales();
                $prioriterizedLocalesConditions = [];
                foreach ($localesRaw as $locale) {
                    if ($selectedLanguage == $locale->code) {
                        $prioriterizedLocalesConditions[0] = 'WHEN loc1 = ' . $locale->id . ' THEN 1';
                        continue;
                    }
                    if ($defaultLanguage == $locale->code) {
                        $prioriterizedLocalesConditions[1] = 'WHEN loc1 = ' . $locale->id . ' THEN 2';
                        continue;
                    }
                }
                $orderCondition = '';
                if (!empty($prioriterizedLocalesConditions)) {
                    $orderQuery = '';
                    foreach ($prioriterizedLocalesConditions as $condition){
                        $orderQuery .= $condition . ' ';
                    }
                    $orderQuery .= 'ELSE 3 ';
                    $orderCondition = ' ORDER BY CASE ' . $orderQuery . 'END ASC, CASE ' . str_replace('loc1', 'loc2', $orderQuery) . ' END ASC';
                }
                $courseData = DB::select("SELECT tmp.course_id, `description`, title, seo_url, price FROM (SELECT cr.course_id, IF(tl.is_default = 0, JSON_EXTRACT(ta.attribute_data, '$.description'), cr.description) AS `description`, pc.title, pc.seo_url, pc.price, tl.id AS loc1, pc.locale_id AS loc2 FROM greenway_courses_city_related cr LEFT JOIN rainlab_translate_locales tl ON (tl.is_enabled = 1) LEFT JOIN rainlab_translate_attributes ta ON (ta.model_type = 'Greenway\\\\Courses\\\\Models\\\\CityRelation' AND ta.model_id = cr.id AND ta.locale = tl.code) LEFT JOIN greenway_courses_pub_courses pc ON (pc.source_id = cr.course_id AND pc.published_at < NOW() AND pc.deleted_at IS NULL) WHERE cr.city_id = ?) tmp LEFT JOIN greenway_courses_payments p ON (p.user_id = ? AND p.course_id = tmp.course_id) WHERE `description` IS NOT NULL AND title != '' AND p.course_id IS NULL" . $orderCondition . " LIMIT 1", [$cityRelatedOfferCond[0]->city_id, $user->id]);
                if (!empty($courseData)){
                    $courseData = $courseData[0];
                    $crOfferData['selectedLanguage'] = $selectedLanguage;
                    if ($selectedLanguage == $defaultLanguage) {
                        $crOfferData['description'] = $courseData->description;
                    } else {
                        $crOfferData['description'] = trim(stripcslashes($courseData->description), '"');
                    }
                    if ($_SERVER['HTTP_HOST'] == 'green-way.com.ua') {
                        $portmoneData = Config::get('greenway.userextension::portmoneData');
                        $pmindex = Config::get('greenway.userextension::portmoneIndex');
                        $crOfferData['pmpayee_id'] = $portmoneData[$pmindex]['portmonePayeeId'];
                    } else {
                        $crOfferData['pmpayee_id'] = Config::get('greenway.userextension::testPortmonePayeeId');
                    }
                    $crOfferData['cName'] = $courseData->title;
                    $crOfferData['oldPrice'] = $courseData->price;
                    $crOfferData['price'] = round($courseData->price / 2.0, 1);
                    if (!empty($courseData->seo_url)){
                        $crOfferData['courseUrl'] = $courseData->seo_url;
                    } else {
                        $crOfferData['courseUrl'] = $courseData->course_id;
                    }
                    if (!empty($cityRelatedOfferCond[0]->pr1)){
                        $promocodeId = $cityRelatedOfferCond[0]->pr1;
                    } elseif (!empty($cityRelatedOfferCond[0]->pr2)){
                        $promocodeId = $cityRelatedOfferCond[0]->pr2;
                    } else {
                        $promocodeId = 0;
                    }
                    $crOfferData['orderNumber'] = $user->id . '-' . $courseData->course_id . '-' . $promocodeId;
                    $crOffer = $this->renderPartial('cityRelatedCourse.htm', [
                        'description' => $crOfferData['description'],
                        'oldPrice' => $crOfferData['oldPrice'],
                        'price' => $crOfferData['price'],
                        'pmpayee_id' => $crOfferData['pmpayee_id'],
                        'orderNumber' => $crOfferData['orderNumber'],
                        'cName' => $crOfferData['cName'],
                        'selectedLanguage' => $crOfferData['selectedLanguage'],
                        'courseUrl' => $crOfferData['courseUrl'],
                    ]);
                    $affected = DB::update("UPDATE greenway_userextension_users_extra SET cr_course = DATE_ADD(NOW(), INTERVAL 1 DAY) WHERE user_id = ?", [$user->id]);
                    if (array_key_exists('userData', $_COOKIE)){
                        $cData = json_decode($_COOKIE['userData']);
                    } else {
                        $cData = new \StdClass;
                    }
                    $cData->cRC = 0;
                    setcookie('userData', json_encode($cData), time() + (60 * 60 * 24 * 365 * 20), '/');
                    $_COOKIE['userData'] = json_encode($cData);
                }
            }
        } else {
            $affected = DB::update("UPDATE greenway_education_connection_info SET is_completed = 0, completed_at = ?, is_cookie = 0 WHERE id = ?",[Request::server('REQUEST_TIME'), $testingData['sessionId']]);
        }
        if($answersData['qInRow'] > 4) {
            $rewards[] = 'exam-5-in-row';
            if ($answersData['qInRow'] > 6) {
                $rewards[] = 'exam-7-in-row';
                if ($answersData['qInRow'] > 9) {
                    $rewards[] = 'exam-10-in-row';
                    if ($answersData['qInRow'] > 14) {
                        $rewards[] = 'exam-15-in-row';
                        if ($answersData['qInRow'] > 17) {
                            $rewards[] = 'exam-18-in-row';
                        }
                    }
                }
            }
        }
        $oldRatingData = TestpddMod::getRatingDataCountry($user->id);
        TestpddMod::releaseSecondaryCookie($user->id);
        TestpddMod::releaseRewards('E', $user->id, $rewards, $spendTime, $answersData['totalRA'], $answersData['totalWA']);
        TestpddMod::clearPrimaryCookie($user->id);
        $newRatingData = TestpddMod::getRatingDataCountry($user->id);
        $pointsDifference = $newRatingData['userPoints'] - $oldRatingData['userPoints'];
        $ratingDifference = $oldRatingData['pointsPosTotal'] - $newRatingData['pointsPosTotal'];
        $gotAchievements = [];
        $gotRewards = DB::select("SELECT gur.achievment_id FROM greenway_userextension_rewards gur LEFT JOIN greenway_userextension_achievments gua ON (gur.achievment_id = gua.id) WHERE gur.user_id = ? AND gur.counter >= gua.achieves_count AND gur.status IS NULL AND gua.name != '' AND gua.is_active = 1 AND gua.is_released AND gua.is_system = 0", [$user->id]);
        $ids = [];
        foreach ($gotRewards as $reward){
            $ids[] = $reward->achievment_id;
        }
        if (!empty($ids)) {
            $gotAchievementsList = Achievements::whereIn('id', $ids)->get();
            foreach ($gotAchievementsList as $achievement){
                $achievementData['id'] = $achievement->id;
                $achievementData['name'] = $achievement->name;
                $achievementData['conditions'] = $achievement->conditions;
                $achievementData['image'] = $achievement->image->path;
                array_push($gotAchievements, $achievementData);
            }
            DB::update('UPDATE greenway_userextension_rewards SET status = 1 WHERE user_id = ? AND achievment_id IN (' . implode(",", $ids) . ')', [$user->id]);
        }
        return ['ratingData' => ['pointsDifference' => $pointsDifference, 'ratingPos' => $newRatingData['pointsPosTotal'], 'ratingDifference' => $ratingDifference, 'gotAchievements' => $gotAchievements], 'crOffer' => $crOffer];
    }
    public function onAnswerMade(){
        $user = Auth::getUser();
        if (!$user){
            return ['error' => 'nonunique account'];
        }
        $validation = Validator::make(post(), [
            'qid' => 'required|numeric',
            'aid' => 'required|numeric'
        ]);
        if ($validation->fails()) {
            return ['error' => $validation->failed()];
        }
        $qid = post('qid');
        $rightA = Cache::remember('q' . $qid . 'rA', 1440, function() use($qid){
            $raId = DB::select("SELECT id FROM greenway_testpdd_answers WHERE question_id = ? AND is_correct = 1 LIMIT 1", [$qid]);
            return !empty($raId) ? $raId[0]->id : 0;
        });
        session_start();
        if (array_key_exists('answerStack', $_SESSION)){
            $ansStack = $_SESSION['answerStack'];
        }
        $ansStack[] = array($qid, $rightA == post('aid'));
        $_SESSION['answerStack'] = $ansStack;
        return ['success' => array('qid' => $qid, 'aid' => post('aid'), 'raid' => $rightA)];
    }
    public function onGetLikes(){
        $qids = post('qids');
        if (empty($qids)){
            return ['totalLikes' => [], 'userLikes' => []];
        }
        $totalLikes = TestpddMod::getTotalLikes('question', $qids);
        $user = Auth::getUser();
        if (!$user){
            return ['totalLikes' => $totalLikes, 'userLikes' => []];
        }
        $userLikes = TestpddMod::getUserLikes('question', $qids, $user->id);
        return ['totalLikes' => $totalLikes, 'userLikes' => $userLikes];
    }
    public function onGetFavorites(){
        $user = Auth::getUser();
        $qids = post('qids');
        if ((!$user)||(empty($qids))){
            return ['userFavorites' => []];
        }
        $userFavorites = TestpddMod::getUserFavorites($qids, $user->id, 'question');
        return ['userFavorites' => $userFavorites];
    }
    public function onSendTeacher()
    {
        $user = Auth::getUser();
        if (!$user){
            return;
        }
        $validation = Validator::make(post(), [
            'tname' => 'required|max:255|regex:/^[ a-zA-Z0-9а-яА-Я,ЙйЦцУуКкЕеНнГгШшЩщЗзХхФфЫыВвАаПпРрОоЛлДдЖжЭэЯяЧчСсМмИиТтЬьБбЮюёЁыЫэЭ,а-яіїєґ,А-ЯІЇЄҐ\\\'\s\.,]+$/i'
        ]);
        if ($validation->fails()) {
            return ['error' => $validation->failed()];
        }
        $student = DB::select("SELECT u.name, u.email, guue.phone, guc.text AS city, gur.text AS region, gua.name AS autoschoolName, aguc.text AS autoschoolCity, agur.text AS autoschoolRegion FROM users u LEFT JOIN greenway_userextension_users_extra guue ON u.id = guue.user_id LEFT JOIN greenway_userextension_cities guc ON guue.city_id = guc.id LEFT JOIN greenway_userextension_regions gur ON guc.region_id = gur.id LEFT JOIN greenway_userextension_autoschools gua ON guue.autoschool_id = gua.id LEFT JOIN greenway_userextension_cities aguc ON gua.city_id = aguc.id LEFT JOIN greenway_userextension_regions agur ON aguc.region_id = agur.id WHERE u.id = " . $user->id . " LIMIT 1");
        $data = [
            'studentName'      => $student[0]->name,
            'studentCity'      => $student[0]->city,
            'studentRegion'    => $student[0]->region,
            'studentPhone'     => $student[0]->phone,
            'studentEmail'     => $student[0]->email,
            'teacherName'      => post('tname'),
            'teacherNumber'    => post('tnumber'),
            'autoschoolName'   => $student[0]->autoschoolName,
            'autoschoolCity'   => $student[0]->autoschoolCity,
            'autoschoolRegion' => $student[0]->autoschoolRegion,
        ];
        if ($_SERVER['HTTP_HOST'] == 'green-way.com.ua') {
            $infoMail = Config::get('greenway.mailbuilder::infoMail');
        } else {
            $infoMail = Config::get('greenway.mailbuilder::testInfoMail');
        }
        Mail::send('greenway.education::mail.ru.teacher', $data, function ($message) use ($infoMail) {
            $message->to($infoMail, 'info');
            $message->priority(2);
        });
    }
}